# Error Numbers

01: Operation not permitted  
02: No such file or directory  
03: No such process  
04: Interrupted function call  
05: Input/output error  
06: No such device or address  
07: Arg list too long  
08: Exec format error  
09: Bad file descriptor  
10: No child processes  
11: Resource temporarily unavailable  
12: Not enough space  
13: Permission denied  
14: Bad address  
16: Resource device  
17: File exists  
18: Improper link  
19: No such device  
20: Not a directory  
21: Is a directory  
22: Invalid argument  
23: Too many open files in system  
24: Too many open files  
25: Inappropriate I/O control operation  
27: File too large  
28: No space left on device  
29: Invalid seek  
30: Read-only file system  
31: Too many links  
32: Broken pipe  
33: Domain error  
34: Result too large  
36: Resource deadlock avoided  
38: Filename too long  
39: No locks available  
40: Function not implemented  
41: Directory not empty  
42: Illegal byte sequence  
