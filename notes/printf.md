# int printf(const char * format, ...)

The function printf sends formatted text to stdout.

## Parameters

The first parameter is the format string which may contain format
specifiers. After this any parameters are placed in the string at
the specifiers. The parameters are used in order unless specified
in the specifier. Be careful as printf does not check if you use
the right amount so strange behaviour can occur.

### Specifiers

The syntax for a format specifier is:
``%[flags][width][.precision][length]specifier``

The specifier is the most important part as it defines how the argument is
interpreted. These specifiers are:

| Specifier | Output                  |
|-----------|-------------------------|
| ``d, i``  | Signed int              |
| ``u``     | Unsigned int            |
| ``o``     | Unsigned octal          |
| ``x`` *   | Unsigned hex int        |
| ``f`` *   | Floating point          |
| ``e`` *   | Scientific notation     |
| ``a`` *   | Hex scientific notation |
| ``g`` *   | Uses shortest of e or f |
| ``c``     | Character               |
| ``s``     | String                  |
| ``p``     | Pointer address         |
| ``%%``    | %                       |

__*__ these specifiers have a lower and uppercase variant.

### Flags

- ``(-)`` Left justify in the given width, right justify is the default  
- ``(+)`` Forces a negative or positive sign for numbers  
- ``( )`` Positive numbers will be followed by a space  
- ``(0)`` Left pads numbers with zeros if padding
- ``(#)`` With o, x, or X the number will be followed by 0, 0x, or 0X.
          With a, e, f, or g the output will always have a decimal point

### Width

The width determines the minimum number of characters to be printed. If the
value is shorter than the width the result is padded with spaces. If you use
``*`` the width goes in the parameters.

### Precision

For integer specifiers this specifies the minimum number of digits to be
printed, and if the value is shorter it will be padded with zeroes. The value
will not be truncated and a precision of 0 means that no characters will be
written for the number 0.
For non-integers (a, e, f) this specifies the amount of digits after the
decimal point to be printed. By default 6 will be printed.

For (g) this specifies the amount of significant digits to be printed.  
For (s) this specifies the amount of characters to be printed.  
If the period is used without a value, 0 is assumed.

with ``.*`` the precision is used in the parameters.  

## Output

The function outputs the number of characters written on success, and a
negative number on error.
